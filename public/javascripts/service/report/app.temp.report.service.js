/**
 * 
 */
(function(angular){
	"use strict";
	angular.module("dynreport").service("ngTempServ", ["ngHttpService", function(ngHttpService){
		return{
			data : function(url){
				return ngHttpService.get(url);
			}
		}
	}]);
})(window.angular);