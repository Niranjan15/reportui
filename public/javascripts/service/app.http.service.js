/**
 * 
 */
(function(angular){
	"use script";
	
	angular.module('dynreport').factory('ngHttpService',[ '$http', '$q', function( $http, $q){
		var commonHeader = {headers : {'Content-Type' : 'application/json'}};
		return {
			post : function(url, data){

				var deferred = $q.defer();
				$http.post((url), data, commonHeader).then(function(resp){
					if(resp.data.status == 'success')
					{
						deferred.resolve(resp.data.result);
					}
					else
					{
						deferred.reject(resp.data.result);
					}
				}, function(err){
					deferred.reject(err);
				});
				return deferred.promise;
				
			},
			get : function(url){
				
				var deferred = $q.defer();
				$http.get(url, commonHeader).then(function(resp){
					if(resp.data.status == 'success')
					{
						deferred.resolve(resp.data.result);
					}
					else
					{
						deferred.reject(resp.data.result);
					}
				}, function(err){
					deferred.reject(err);
				});
				return deferred.promise;
				
			},
			uploadFile : function(file, uploadUrl){
				
				var deferred = $q.defer();
				var fd = new FormData();
				fd.append('file', file);
            
				$http.post(uploadUrl, fd, {
					transformRequest: angular.identity,
					headers: {'Content-Type': undefined}
				}).then(function(resp){
					if(resp.data.status == 'success')
					{
						deferred.resolve(resp.data.result);
					}
					else
					{
						deferred.reject(resp.data.result);
					}
				}, function(err){
            	   deferred.reject(err);
				});
				return deferred.promise;
				
			},
			uploadFiles : function(files, uploadUrl){
				
				var deferred = $q.defer();
				var fd = new FormData();
				for(var i = 0; i < files.length; i++)
				{
					fd.append('file', files[i]);
				}
            
				$http.post(UrlsConst.CONTEXT + uploadUrl, fd, {
					transformRequest: angular.identity,
					headers: {'Content-Type': undefined}
				}).then(function(resp){
					if(resp.data.status == 'success')
					{
						deferred.resolve(resp.data.result);
					}
					else
					{
						deferred.reject(resp.data.result);
					}
				}, function(err){
            	   deferred.reject(err);
				});
				return deferred.promise;
				
			}
		};
	}]);
	
})(window.angular);