/**
 * 
 */
(function(angular){
	"use strict";
	angular.module('HighChart', [])
    // Directive for generic chart, pass in chart options
    .directive('hcChart',["$timeout", function ($timeout) {
        return {
            restrict: 'E',
            template: '<div></div>',
            scope: {
            	options : '='
            },
            link: function (scope, element) {
            	Highcharts.chart(scope.itemid, scope.options);
            }
        };
    }]);
})(window.angular);