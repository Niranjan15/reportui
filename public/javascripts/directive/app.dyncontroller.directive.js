/**
 * 
 */
(function(angular){
	"use strict";
	angular.module("dynreport").directive("ngDynCtrl",['$compile', '$parse',function($compile, $parse) {
		  return {
		      scope: {
		          name: '=ngDynCtrl'
		      },
		      restrict: 'AE',
		      terminal: true,
		      priority: 100000,
		      link: function(scope, elem, attrs) {
		    	  elem.removeAttr('ng-dyn-ctrl');
		          elem.attr('ng-controller', scope.name);
		          scope.widget = scope.$parent.$parent.item;
		          $compile(elem)(scope);
		      }
		  };
		}]);
})(window.angular)