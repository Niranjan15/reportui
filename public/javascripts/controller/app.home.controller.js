/**
 * 
 */
(function(angular){
	"use strict";
	angular.module("dynreport").controller("ngHomeCtrl", ["ngHomeServ", "$scope", "$timeout", "$mdDialog",
		function(ngHomeServ, $scope, $timeout, $mdDialog){
		
		$scope.gridsterOpts = {
				columns : 20,
				margin : [50,50],
				isMobile: true,
				mobileBreakPoint: 600, 
				mobileModeEnabled: false,
				swapping : false, 
				minSizeX : 5,
				minSizeY : 5,
				pushing: true,
				resizable: {
					   enabled: true,
					   handles: ['n', 'e', 's', 'w', 'ne', 'se', 'sw', 'nw'],
					   start: function(event, $element, widget) {
						   $("#"+widget.id).highcharts().setSize($element[0].clientWidth, $element[0].clientHeight, false);
					   }, // optional callback fired when resize is started,
					   resize: function(event, $element, widget) {
						   $("#"+widget.id).highcharts().setSize($element[0].clientWidth, $element[0].clientHeight, false);
					   }, // optional callback fired when item is resized,
					   stop: function(event, $element, widget) {
						   $timeout(function(){
							   $("#"+widget.id).highcharts().setSize($element[0].clientWidth, $element[0].clientHeight, false);
						   }, 500);
					   } // optional callback fired when item is finished resizing
					},
			};
		var menuContext = [
			/*{
	        text: 'Edit',
	        click: function ($itemScope, $event, modelValue, text, $li) {
	        	$scope.showPrompt($event);
	        }
	    },*/
	    {
	        text: 'Remove',
	        click: function ($itemScope, $event, modelValue, text, $li) {
	        	angular.forEach($scope.standardItems, function(item, index){
	        		if(item.id == $itemScope.widget.id){
	        			$scope.standardItems.splice(index, 1);
	        		}
	        	});
	        }
	    }];
		var contextUrl = "https://reportmicroservice.run.aws-usw02-pr.ice.predix.io/";
//		var contextUrl = "http://localhost:8080/"
		$scope.standardItems = [
			  {id :"container1", controller: "ngTempCtrl", sizeX: 20, sizeY: 5, row: 21, col: 0, menuOptions : menuContext, url : contextUrl + "area"},
			  {id :"container2", controller: "ngTempCtrl", sizeX: 10, sizeY: 5, row: 6, col: 0, menuOptions : menuContext, url : contextUrl + "line"},
			  {id :"container3", controller: "ngTempCtrl", sizeX: 10, sizeY: 5, row: 6, col: 11, menuOptions : menuContext, url : contextUrl + "bar"},
			  {id :"container4", controller:"ngTempCtrl", sizeX: 10, sizeY: 5, row: 11, col: 0, menuOptions : menuContext, url : contextUrl + "pie"},
			  {id :"container5", controller:"ngTempCtrl", sizeX: 10, sizeY: 5, row: 11, col: 11, menuOptions : menuContext, url : contextUrl + "column"},
			  {id :"container6", controller:"ngTempCtrl", sizeX: 10, sizeY: 5, row: 16, col: 0, menuOptions : menuContext, url : contextUrl + "bubble"},
			  {id :"container7", controller:"ngTempCtrl", sizeX: 10, sizeY: 5, row: 16, col: 11, menuOptions : menuContext, url : contextUrl + "threed"},
//			  {id :"container8", controller:"ngTempCtrl", sizeX: 20, sizeY: 5, row: 23, col: 0, menuOptions : menuContext, url : contextUrl + "gauges"},
			  {id :"container8", controller:"ngTempCtrl", sizeX: 20, sizeY: 5, row: 0, col: 0, menuOptions : menuContext, url : contextUrl + "combo"},
			];
		var counter = 9;
		$scope.addWidget = function () {
			$scope.standardItems.push({id :"container"+(counter++), controller: "ngTempCtrl", sizeX: 10, sizeY: 5, row: 0, col: 0, menuOptions : menuContext, url : contextUrl + "area"});
		};
		
		$scope.showPrompt = function(ev) {
		    // Appending dialog to document.body to cover sidenav in docs app
		    var confirm = $mdDialog.prompt()
		      .title('')
		      .textContent('')
		      .placeholder('')
		      .ariaLabel('Dog name')
		      .initialValue('Buddy')
		      .targetEvent(ev)
		      .required(true)
		      .ok('Okay!')
		      .cancel('Cancel');

		    $mdDialog.show(confirm).then(function(result) {
		      $scope.status = 'You decided to name your dog ' + result + '.';
		    }, function() {
		      $scope.status = 'You didn\'t name your dog.';
		    });
		  };
	}]);
})(window.angular);