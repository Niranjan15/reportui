/**
 * 
 */
(function(angular){
	"use strict";
	angular.module("dynreport").controller("ngTempCtrl", ["$scope", "$timeout", "ngTempServ", function($scope, $timeout, ngTempServ){
		$scope.init = function(){
			$scope.widget = $scope.$parent.widget;
			$timeout(function(){
				ngTempServ.data($scope.widget.url).then(function(data){
					
					try{
						Highcharts.chart($scope.widget.id, JSON.parse(data));
					}
					catch(e){
						console.error(e);
						console.log(data);
					}
				});
			}, 500);
		};
	}]);
})(window.angular);