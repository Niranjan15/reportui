/**
 * 
 */
(function(angular){
	"use strict";
	angular.module("dynreport", ["ui.router", "gridster", "HighChart", "ui.bootstrap.contextMenu", "ngMaterial"]);
	
})(window.angular);