/**
 * 
 */
(function(angular){
	"use strict";
	angular.module("dynreport").config(function($stateProvider, $urlRouterProvider){
		
		var rootState = {
			name : "root",
//			url: '',
	        abstract: true,
	        views: {
	              '@' : {
	                templateUrl: '../public/pages/base/LayoutScreen.html'
	              },
	              'header@root' : { templateUrl: '../public/pages/base//HeaderScreen.html',},
	              'footer@root' : { templateUrl: '../public/pages/base//FooterScreen.html',},
	            },
		};
		
		var homeState = {
			name : "root.home",
			url : '/',
			views : {
				'container@root' : {
					 templateUrl: '../public/pages/base//HomeScreen.html',
		             controller: 'ngHomeCtrl'
				}
			}
		};
		
		$stateProvider.state(rootState);
		$stateProvider.state(homeState);
		$urlRouterProvider.otherwise('/');
	});
})(window.angular);